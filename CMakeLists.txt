cmake_minimum_required(VERSION 3.5)

project(bindings)


set(CMAKE_BUILD_TYPE "Release")
set(CMAKE_VERBOSE_MAKEFILE On)

add_library(sfoo STATIC bindings.cc)
target_compile_options(sfoo PRIVATE -fPIC -acc -ta=tesla:nordc -Minfo=accel)

add_library(dfoo SHARED bindings.cc)
target_compile_options(sfoo PRIVATE -fPIC -acc -ta=tesla:nordc -Minfo=accel)

set(CMAKE_INSTALL_PREFIX "${CMAKE_SOURCE_DIR}/install")
install(TARGETS sfoo dfoo DESTINATION lib)
