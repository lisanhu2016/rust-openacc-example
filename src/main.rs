use rust_openacc_example::bindings::*;

use rand::{thread_rng, Rng};

fn main() {
    let mut rng = thread_rng();
    let mut values: Vec<i32> = vec![];
    const LENGTH: u64 = 100_000_000;
    values.reserve(LENGTH as usize);
    for _ in 0..LENGTH {
        values.push(rng.gen());
    }

    let arr = array::new(&values);
    unsafe {
        process_array(arr);
    }
}
