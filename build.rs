pub fn main() {
    // use std::env;
    let src_dir = env!("CARGO_MANIFEST_DIR");
    let mut path = std::path::PathBuf::from(src_dir);
    path.push("install/lib");
    println!("cargo:rustc-link-search={}", path.to_str().unwrap());
    
    println!("cargo:rustc-link-lib=dylib=acchost");
    println!("cargo:rustc-link-lib=dylib=accdevice");
    println!("cargo:rustc-link-lib=dylib=nvhpcman");
    println!("cargo:rustc-link-lib=dylib=dl");
    println!("cargo:rustc-link-lib=dylib=cudadevice");
    println!("cargo:rustc-link-lib=dylib=atomic");
    println!("cargo:rustc-link-lib=dylib=nvhpcatm");
    println!("cargo:rustc-link-lib=dylib=stdc++");
    println!("cargo:rustc-link-lib=dylib=nvomp");
    println!("cargo:rustc-link-lib=dylib=dl");
    println!("cargo:rustc-link-lib=dylib=nvhpcatm");
    println!("cargo:rustc-link-lib=dylib=atomic");
    println!("cargo:rustc-link-lib=dylib=pthread");
    println!("cargo:rustc-link-lib=dylib=nvcpumath");
    println!("cargo:rustc-link-lib=dylib=nsnvc");
    println!("cargo:rustc-link-lib=dylib=nvc");
    println!("cargo:rustc-link-lib=dylib=m");
    println!("cargo:rustc-link-lib=dylib=gcc");
    println!("cargo:rustc-link-lib=dylib=c");
    println!("cargo:rustc-link-lib=dylib=gcc");
    println!("cargo:rustc-link-lib=dylib=gcc_s");
    println!("cargo:rustc-link-lib=dylib=nvcpumath");

}
