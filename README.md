# A simple example to call OpenACC GPU code from Rust-lang

## Introduction

This repository is serving as a very basic and naive example to call OpenACC routine built with CMake from Rust code. Currently, it will be able to compile and link, ~~however the code won't run on GPU right now. This question has been asked on stackoverflow at `https://stackoverflow.com/questions/63758643/calling-openacc-code-from-rust-does-not-run-on-gpu`~~, and run on GPU.

## Steps to reproduce
1. download and install rust compiler and build system from `https://www.rust-lang.org/tools/install`
2. clone this project 
3. go to the project folder and create a folder called `build`
4. set up environment to load nvhpc/20.7 to your current system (Remember to check whether the compiler lib folder is within both your `LIBRARY_PATH`, and `LD_LIBRARY_PATH` environment variables, nvhpc is required, if you are using the older versions, you may need to change the flags in `CMakeLists.txt`, and change the library names in `build.rs` to the names used in the older versions)
5. in the `build` folder, build and install, the CMake project will create a `install/lib` folder under source root, and it will be searched by Rust code when linking.
6. go to source root and run `cargo build --release`, it should compile and link correctly
7. run the code with `cargo run --release`, if only one line of `Done kernel` is shown, it means the code ran correctly. The C++ part is quite simple, you can check its functionality in `bindings.cc`.
8. run the code with `PGI_ACC_TIME=1 cargo run --release`~~, you will see no kernel timing information shown~~.
9. run the code with `ncu --set full cargo run --release`, you will see no kernels were profiled. Probably the kernel is too small.

### How to change the linking libraries
If you want to try linking to other set of shared libraries, make sure `LIBRARY_PATH` and `LD_LIBRARY_PATH` contain the folder, and you can change the contents in `build.rs` file. I'm really sorry, but I don't know how to do the linking without `build.rs`.

### Why using `-fPIC` to compile the `libfoo`?
Because in default build of `gcc` on Ubuntu and some other distributions, the system `cc` is configured with PIE enabled by default. The Rust linker will call system `cc` and I have no control to it whether to use `-no-pie` (The flags you can pass to the linker is restricted to `-L` and the folders). In such cases, if your own code is not compiled with `-fPIE` or `-fPIC`, it won't be able to link. However, `nvc` and `nvc++` couldn't recognize `-fPIE`, so I'm using `-fPIC` here.

### How to compile and link to shared library
In `src/bindings.rs`, there's one line of `#[link(name="sfoo", kind="static")]`. If you checked the `CMakeLists.txt`, you will find there's another shared library version, which is called `libdfoo`, you should change that line to `#[link(name="dfoo")]` to link to it.

### An example of running the above steps is shown here, starting from step 3
```bash
(base) lisanhu@skywalker:~/tmp/rust-openacc-example
$ clear
(base) lisanhu@skywalker:~/tmp/rust-openacc-example
$ ls
bindings.cc  bindings.h  bindings.rs  build.rs  Cargo.lock  Cargo.toml  CMakeLists.txt  README.md  src
(base) lisanhu@skywalker:~/tmp/rust-openacc-example
$ mkdir build
(base) lisanhu@skywalker:~/tmp/rust-openacc-example
$ module add nvhpc
(base) lisanhu@skywalker:~/tmp/rust-openacc-example
$ export LIBRARY_PATH=$LD_LIBRARY_PATH
(base) lisanhu@skywalker:~/tmp/rust-openacc-example
$ cd build
(base) lisanhu@skywalker:~/tmp/rust-openacc-example/build
$ cmake ../
-- The C compiler identification is PGI 20.7.0
-- The CXX compiler identification is PGI 20.7.0
-- Check for working C compiler: /opt/nvidia/hpc/20.7/Linux_x86_64/20.7/compilers/bin/nvc
-- Check for working C compiler: /opt/nvidia/hpc/20.7/Linux_x86_64/20.7/compilers/bin/nvc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: /opt/nvidia/hpc/20.7/Linux_x86_64/20.7/compilers/bin/nvc++
-- Check for working CXX compiler: /opt/nvidia/hpc/20.7/Linux_x86_64/20.7/compilers/bin/nvc++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done
-- Generating done
-- Build files have been written to: /usa/lisanhu/tmp/rust-openacc-example/build
(base) lisanhu@skywalker:~/tmp/rust-openacc-example/build


$ make install
/usa/lisanhu/mine/libs/cmake/bin/cmake -S/usa/lisanhu/tmp/rust-openacc-example -B/usa/lisanhu/tmp/rust-openacc-example/build --check-build-system CMakeFiles/Makefile.cmake 0
/usa/lisanhu/mine/libs/cmake/bin/cmake -E cmake_progress_start /usa/lisanhu/tmp/rust-openacc-example/build/CMakeFiles /usa/lisanhu/tmp/rust-openacc-example/build/CMakeFiles/progress.marks
make -f CMakeFiles/Makefile2 all
make[1]: Entering directory '/usa/lisanhu/tmp/rust-openacc-example/build'
make -f CMakeFiles/dfoo.dir/build.make CMakeFiles/dfoo.dir/depend
make[2]: Entering directory '/usa/lisanhu/tmp/rust-openacc-example/build'
cd /usa/lisanhu/tmp/rust-openacc-example/build && /usa/lisanhu/mine/libs/cmake/bin/cmake -E cmake_depends "Unix Makefiles" /usa/lisanhu/tmp/rust-openacc-example /usa/lisanhu/tmp/rust-openacc-example /usa/lisanhu/tmp/rust-openacc-example/build /usa/lisanhu/tmp/rust-openacc-example/build /usa/lisanhu/tmp/rust-openacc-example/build/CMakeFiles/dfoo.dir/DependInfo.cmake --color=
Scanning dependencies of target dfoo
make[2]: Leaving directory '/usa/lisanhu/tmp/rust-openacc-example/build'
make -f CMakeFiles/dfoo.dir/build.make CMakeFiles/dfoo.dir/build
make[2]: Entering directory '/usa/lisanhu/tmp/rust-openacc-example/build'
[ 25%] Building CXX object CMakeFiles/dfoo.dir/bindings.cc.o
/opt/nvidia/hpc/20.7/Linux_x86_64/20.7/compilers/bin/nvc++  -Ddfoo_EXPORTS  -fast -O3 -DNDEBUG -fPIC   -o CMakeFiles/dfoo.dir/bindings.cc.o -c /usa/lisanhu/tmp/rust-openacc-example/bindings.cc
[ 50%] Linking CXX shared library libdfoo.so
/usa/lisanhu/mine/libs/cmake/bin/cmake -E cmake_link_script CMakeFiles/dfoo.dir/link.txt --verbose=1
/opt/nvidia/hpc/20.7/Linux_x86_64/20.7/compilers/bin/nvc++ -fPIC -fast -O3 -DNDEBUG  -shared -Wl,-soname,libdfoo.so -o libdfoo.so CMakeFiles/dfoo.dir/bindings.cc.o
make[2]: Leaving directory '/usa/lisanhu/tmp/rust-openacc-example/build'
[ 50%] Built target dfoo
make -f CMakeFiles/sfoo.dir/build.make CMakeFiles/sfoo.dir/depend
make[2]: Entering directory '/usa/lisanhu/tmp/rust-openacc-example/build'
cd /usa/lisanhu/tmp/rust-openacc-example/build && /usa/lisanhu/mine/libs/cmake/bin/cmake -E cmake_depends "Unix Makefiles" /usa/lisanhu/tmp/rust-openacc-example /usa/lisanhu/tmp/rust-openacc-example /usa/lisanhu/tmp/rust-openacc-example/build /usa/lisanhu/tmp/rust-openacc-example/build /usa/lisanhu/tmp/rust-openacc-example/build/CMakeFiles/sfoo.dir/DependInfo.cmake --color=
Scanning dependencies of target sfoo
make[2]: Leaving directory '/usa/lisanhu/tmp/rust-openacc-example/build'
make -f CMakeFiles/sfoo.dir/build.make CMakeFiles/sfoo.dir/build
make[2]: Entering directory '/usa/lisanhu/tmp/rust-openacc-example/build'
[ 75%] Building CXX object CMakeFiles/sfoo.dir/bindings.cc.o
/opt/nvidia/hpc/20.7/Linux_x86_64/20.7/compilers/bin/nvc++    -fast -O3 -DNDEBUG   -fPIC -acc -ta=tesla:nordc -Minfo=accel -o CMakeFiles/sfoo.dir/bindings.cc.o -c /usa/lisanhu/tmp/rust-openacc-example/bindings.cc
process_array:
      6, Generating copyin(data) [if not already present]
         Generating copyout(results[:data.length]) [if not already present]
         Generating copyin(data.data[:data.length]) [if not already present]
         Generating Tesla code
          9, #pragma acc loop gang, vector(128) /* blockIdx.x threadIdx.x */
[100%] Linking CXX static library libsfoo.a
/usa/lisanhu/mine/libs/cmake/bin/cmake -P CMakeFiles/sfoo.dir/cmake_clean_target.cmake
/usa/lisanhu/mine/libs/cmake/bin/cmake -E cmake_link_script CMakeFiles/sfoo.dir/link.txt --verbose=1
/usr/bin/ar qc libsfoo.a  CMakeFiles/sfoo.dir/bindings.cc.o
/usr/bin/ranlib libsfoo.a
make[2]: Leaving directory '/usa/lisanhu/tmp/rust-openacc-example/build'
[100%] Built target sfoo
make[1]: Leaving directory '/usa/lisanhu/tmp/rust-openacc-example/build'
/usa/lisanhu/mine/libs/cmake/bin/cmake -E cmake_progress_start /usa/lisanhu/tmp/rust-openacc-example/build/CMakeFiles 0
make -f CMakeFiles/Makefile2 preinstall
make[1]: Entering directory '/usa/lisanhu/tmp/rust-openacc-example/build'
make[1]: Nothing to be done for 'preinstall'.
make[1]: Leaving directory '/usa/lisanhu/tmp/rust-openacc-example/build'
Install the project...
/usa/lisanhu/mine/libs/cmake/bin/cmake -P cmake_install.cmake
-- Install configuration: "Release"
-- Installing: /usa/lisanhu/tmp/rust-openacc-example/install/lib/libsfoo.a
-- Installing: /usa/lisanhu/tmp/rust-openacc-example/install/lib/libdfoo.so


(base) lisanhu@skywalker:~/tmp/rust-openacc-example/build
$ NV_ACC_TIME=1 cargo run --release
   Compiling libc v0.2.76
   Compiling getrandom v0.1.14
   Compiling cfg-if v0.1.10
   Compiling ppv-lite86 v0.2.9
   Compiling rust-openacc-example v0.1.0 (/usa/lisanhu/tmp/rust-openacc-example)
   Compiling rand_core v0.5.1
   Compiling rand_chacha v0.2.2
   Compiling rand v0.7.3
    Finished release [optimized] target(s) in 4.69s
     Running `/usa/lisanhu/tmp/rust-openacc-example/target/release/rust-openacc-example`
Done kernel

Accelerator Kernel Timing data
/usa/lisanhu/tmp/rust-openacc-example/bindings.cc
  process_array  NVIDIA  devicenum=0
    time(us): 61,519
    6: compute region reached 1 time
        6: kernel launched 1 time
            grid: [1024]  block: [128]
             device time(us): total=1,151 max=1,151 min=1,151 avg=1,151
            elapsed time(us): total=1,186 max=1,186 min=1,186 avg=1,186
    6: data region reached 2 times
        6: data copyin transfers: 26
             device time(us): total=30,618 max=1,284 min=5 avg=1,177
        11: data copyout transfers: 24
             device time(us): total=29,750 max=1,260 min=1,058 avg=1,239


(base) lisanhu@skywalker:~/tmp/rust-openacc-example/build
$ ncu cargo run --release
    Finished release [optimized] target(s) in 0.49s
     Running `/usa/lisanhu/tmp/rust-openacc-example/target/release/rust-openacc-example`
Done kernel
==WARNING== No kernels were profiled.
==WARNING== Profiling kernels launched by child processes requires the --target-processes all option.
(base) lisanhu@skywalker:~/tmp/rust-openacc-example/build
$
```