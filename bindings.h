#if !defined(BINDINGS_H)
#define BINDINGS_H

typedef unsigned long size_t;

typedef struct array {
    int *data;
    size_t length;
} array;

#if defined(__cplusplus)
extern "C" {
#endif  // __cplusplus
void process_array(array data);
#if defined(__cplusplus)
}
#endif  // __cplusplus

#endif  // BINDINGS_H
