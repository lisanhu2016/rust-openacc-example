#include "bindings.h"

#include <cstdio>

void process_array(array data) {
    int *results = new int[data.length];

#pragma acc parallel loop copyin(data, data.data[:data.length]) copyout(results[:data.length])
    for (size_t i = 0; i < data.length; ++i) {
        results[i] = data.data[i] << 1;
    }

    for (size_t i = 0; i < data.length; ++i) {
        if (results[i] != data.data[i] << 1) {
            printf("Error found at i=%lu\n", i);
        }
    }

    printf("Done kernel\n");

    delete results;
}
